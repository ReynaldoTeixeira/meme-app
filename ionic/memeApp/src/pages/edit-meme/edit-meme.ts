import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EditMemePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-meme',
  templateUrl: 'edit-meme.html',
})
export class EditMemePage {

  public imgMeme_url; //variable that receive each url clicked of HomePage
  textForMemes = [];
  textMeme:string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.imgMeme_url = this.navParams.get('img_url');//receiving the meme img of Home Page
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditMemePage');
  }

  sendText(){
    this.textForMemes.push(this.textMeme);
    this.textMeme = "";
    console.log(this.textForMemes);
  }
}
