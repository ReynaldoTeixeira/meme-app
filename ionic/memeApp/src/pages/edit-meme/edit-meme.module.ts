import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditMemePage } from './edit-meme';
import { AngularDraggableModule } from 'angular2-draggable';

@NgModule({
  declarations: [
    EditMemePage,
  ],
  imports: [
    IonicPageModule.forChild(EditMemePage),
    AngularDraggableModule
  ],
})
export class EditMemePageModule {}
