import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ApiMemeGeneratorProvider } from '../../providers/api-meme-generator/api-meme-generator';
import { EditMemePage } from '../edit-meme/edit-meme';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[
    ApiMemeGeneratorProvider
  ]
})
export class HomePage {

  memeList = new Array<any>(); //variable api(json) ready to use 

  constructor(public navCtrl: NavController, private apiMemeGeneratorProvider: ApiMemeGeneratorProvider) {
    // API config
    this.apiMemeGeneratorProvider.getMemes().subscribe( data =>{
      const response = (data as any);
      const obj_return = JSON.parse(response._body);
      this.memeList = obj_return.data;
      console.log(this.memeList); //Just test
    }, error =>{
      console.log(error);
    });
    // end API config
  }

  moveEditPage(url){
    this.navCtrl.push(EditMemePage,{
      img_url : url //send Url of img to Edit Page
    });
  }

}
