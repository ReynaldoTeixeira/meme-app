import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiMemeGeneratorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiMemeGeneratorProvider {

  linkApi_memes = 'https://api.imgflip.com/get_memes';

  constructor(public http: Http) {
    console.log('Hello ApiMemeGeneratorProvider Provider');
  }

//  get api link 
  getMemes(){
    return this.http.get(this.linkApi_memes);
  }

}
